import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, BaseChartDirective } from 'ng2-charts';

export interface DialogData {
  chart: any;
}

@Component({
  selector: 'app-expanded-chart-view',
  templateUrl: './expanded-chart-view.component.html',
  styleUrls: ['./expanded-chart-view.component.scss']
})


export class ExpandedChartViewComponent implements OnInit {

  selectedChart: any;

  constructor(public dialModalRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.selectedChart = this.data.chart;
    console.log(this.selectedChart)
  }

}
