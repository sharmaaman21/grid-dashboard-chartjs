import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandedChartViewComponent } from './expanded-chart-view.component';

describe('ExpandedChartViewComponent', () => {
  let component: ExpandedChartViewComponent;
  let fixture: ComponentFixture<ExpandedChartViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpandedChartViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandedChartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
