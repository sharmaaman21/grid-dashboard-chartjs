import { Component, Input, OnInit, SimpleChanges, ViewChild, ElementRef, AfterViewInit, Renderer2, OnChanges } from '@angular/core';
import { GridsterItem, GridsterConfig, GridsterItemComponent } from 'angular-gridster2';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, BaseChartDirective } from 'ng2-charts';
import { MatDialog } from '@angular/material/dialog';
import { ExpandedChartViewComponent } from '../expanded-chart-view/expanded-chart-view.component';

@Component({
  selector: 'app-grid-dashboard',
  templateUrl: './grid-dashboard.component.html',
  styleUrls: ['./grid-dashboard.component.scss']
})

export class GridDashboardComponent implements OnInit {
  @ViewChild("gridsterItem")
  gridItem!: GridsterItemComponent;
  @Input()
  public item!: GridsterItem;
  @Input()
  public unitHeight!: number;
  metricName: string = '';
  isDarkTheme: boolean = false;
  chart: any;
  historicalChartOptions: any;

  @ViewChild("dashboardGrid", { static: true, read: ElementRef })
  dashboardGrid!: ElementRef;

  chartData: any;
  public item1!: GridsterItem;
  public item2!: GridsterItem;
  public item3!: GridsterItem;
  public item4!: GridsterItem;

  options!: GridsterConfig;
  dashboard!: Array<GridsterItem>;
  loaded!: boolean;
  selectedChartType: any = '';
  selectChartType: any = [
    { displayName: 'Line Chart', value: 'line' },
    { displayName: 'Pie Chart', value: 'pie' },
    { displayName: 'Bar Chart', value: 'bar' },
    { displayName: 'Doughnut Chart', value: 'doughnut' },
    { displayName: 'Radar Chart', value: 'radar' }
  ];
  canAddWidget: boolean = true;

  charts: { id: any, data: any, label: any, color: any, chartType: any, legend: any, options: any,}[] = [
    {
      id: '1',
      data: [65, 59, 80, 81, 56, 55, 40],
      label: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
      color: [
        { backgroundColor: ['indigo', 'violet', 'blue', 'green', 'yellow', 'orange', 'red'] }
      ],
      chartType: 'pie',
      legend: true,
      options: {
        scaleShowVerticalLines: false,
        responsive: true,
        title: {
          display: true,
          text: 'Pie Chart Analytics'
        }
      }
    },
    {
      id: '2',
      data: [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A', backgroundColor: 'violet' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B', backgroundColor: 'green' }
      ],
      label: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
      color: [{ backgroundColor: ['blue', 'green'] }],
      chartType: 'line',
      legend: true,
      options: {
        scaleShowVerticalLines: false,
        responsive: true,
        title: {
          display: true,
          text: 'Line Chart Analytics'
        },
        plugins: {
          zoom: {
            pan: {
              enabled: true,
              mode: 'xy'
            },
            zoom: {
              enabled: true,
              drag: true,
              mode: 'xy'
            }
          }
        }
      },
    }
  ];
  lineThickness: any = 2;
  legendPosition: any = 'top';
  stacked: boolean = false;
  count: any = 3;
  lineTension: any = 0.4;
  isStraightLine: boolean = false;
  chartLabels: any;
  chartTitle: string = '';


  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  /**Bar chart config start */
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A', backgroundColor: 'violet' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B', backgroundColor: 'green' }
  ];
  /**Bar chart config end */

  public pieChartType: ChartType = 'pie';
  public pieChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public pieChartData = [120, 150, 180, 90];


  static itemChange(item: any, itemComponent: any) {
    console.log('itemChanged', item, itemComponent);
  }

  static itemResize(item: any, itemComponent: any) {
    console.log('itemResized', item, itemComponent);
  }
  constructor(private rd: Renderer2, public chartModal: MatDialog) {
    this.unitHeight = 0;
    this.item1 = { x: 0, y: 0, rows: 3, cols: 2 };
    this.item2 = { x: 3, y: 3, rows: 2, cols: 2 };
    this.item3 = { x: 5, y: 2, rows: 2, cols: 3 };
    this.item4 = { x: 3, y: 4, rows: 3, cols: 4 };
    this.historicalChartOptions = {
      chart: {
        zoomType: 'x',
        marginRight: 30
      },
      time: {
        useUTC: false
      },
      title: {
        text: 'Graph values for Metric X'
      },
      credits: {
        enabled: false
      },
      subtitle: {
        text: ''
      },
      exporting: { enabled: true },
      xAxis: {
        type: 'datetime',
        title: {
          text: 'Date / Time'
        }
      },
      yAxis: [
        {
          lineWidth: 1,
          title: {
            text: "Metric X"
          }
        }
      ],
      series: [
        {
          name: "Metric X",
          color: 'rgb(31, 119, 180)',
          data: [
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10),
            Math.floor(Math.random() * 10)
          ]
        }
      ]
    };
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (this.loaded) {
      this.resizeChart();
    }
  }

  ngAfterViewInit() {
    console.log(this.dashboardGrid);
  }



  public resizeChart(): void {
    console.log("resizeChart");
    this.historicalChartOptions.chart.height = this.item.rows * (this.unitHeight - 10) + ((this.item.rows - 4) * 10);
    this.historicalChartOptions.chart.width = this.item.cols * (this.unitHeight - 10) + ((this.item.cols - 4) * 10);

    if (this.chart.ref) {
      this.chart.ref.setSize(this.historicalChartOptions.chart.width, this.historicalChartOptions.chart.height, false);
    }
  }
  ngOnInit() {

    this.dashboard = [
      { cols: 1, rows: 6, y: 0, x: 0 },
      { cols: 1, rows: 6, y: 0, x: 0 },
      { cols: 1, rows: 6, y: 0, x: 0 },
      { cols: 1, rows: 4, y: 0, x: 0 },
      { cols: 1, rows: 6, y: 0, x: 0 },
      { cols: 2, rows: 5, y: 0, x: 0 },
      { cols: 1, rows: 5, y: 0, x: 0 },
      { cols: 1, rows: 3, y: 0, x: 0 },

    ];
    this.options = {
      gridType: 'scrollVertical',
      maxRows: 100,
      maxCols: 100,
      maxItemCols: 100,
      maxItemRows: 100,
      maxItemArea: 2500,
      itemChangeCallback: GridDashboardComponent.itemChange,
      itemResizeCallback: GridDashboardComponent.itemResize,
      displayGrid: 'onDrag&Resize',
      // maxRows: 20,
      draggable: {
        enabled: true, // enables to drag element
        // dropOverItems: true, // nothing changes actually
      },
      pushItems: true,
      // disableScrollHorizontal: true, // can drag only to top or bottom
      resizable: { enabled: true }, // enables to resize element
      margin: 10,
      outerMargin: true,
      outerMarginTop: null,
      outerMarginRight: null,
      outerMarginBottom: null,
      outerMarginLeft: null,
      useTransformPositioning: true,
      mobileBreakpoint: 640,
      minCols: 1,
      minRows: 1,
      minItemCols: 1,
      minItemRows: 1,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 105,
      fixedRowHeight: 105,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      enableOccupiedCellDrop: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: false,
      swap: false,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: { north: true, east: true, south: true, west: true },
      pushResizeItems: false,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false
    };

  }

  changedOptions() {
    console.log('changedOptions');
    // this.options.api.optionsChanged();
  }

  removeItem(e: any, item: any) {
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }

  openExpandedView(event: any, chart: any) {
    const dialogRef = this.chartModal.open(ExpandedChartViewComponent, {
      data: {
        chart: chart
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  addItem() {
    console.log(this.chartLabels, typeof this.chartLabels);
    if (this.selectedChartType !== '') {
      this.lineTension = this.isStraightLine ? 0 : this.lineTension;
      let newChartWidget: any = {};
      let chartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
      let chartType: ChartType = this.selectedChartType;
      let chartLegend = true;
      let chartOptions: ChartOptions = {
        responsive: true,
        title: {
          display: true,
          text: this.chartTitle
        },
        legend: {
          position: this.legendPosition  // place legend on the right side of chart
        },
        scales: {
          xAxes: [{
            stacked: this.stacked
          }],
          yAxes: [{
            stacked: this.stacked
          }]
        }
      };
      if (this.selectedChartType === 'bar' || this.selectedChartType === 'radar' || this.selectedChartType === 'line' || this.selectedChartType === 'horizontalBar') {
        let chartDataset: ChartDataSets[];
        chartDataset = [
          {
            data: [65, 59, 80, 81, 56, 55, 40],
            label: 'Series A',
            backgroundColor: 'violet',
            borderWidth: this.lineThickness,
            lineTension: this.lineTension
          },
          {
            data: [28, 48, 40, 19, 86, 27, 90],
            label: 'Series B',
            backgroundColor: 'green',
            borderWidth: this.lineThickness,
            lineTension: this.lineTension
          }
        ];
        newChartWidget['data'] = chartDataset;
      }

      if (this.selectedChartType === 'doughnut' || this.selectedChartType === 'pie') {
        let chartData: SingleDataSet = [];
        chartData = [180, 480, 770, 90, 1000, 270, 400];
        newChartWidget['data'] = chartData;
      }

      newChartWidget['id'] = this.count;
      newChartWidget['label'] = chartLabels;
      newChartWidget['chartType'] = chartType;
      newChartWidget['legend'] = chartLegend;
      newChartWidget['options'] = chartOptions;
      console.log(newChartWidget)
      this.charts.push(newChartWidget);
      this.count = this.count + 1;

      this.canAddWidget = true;
      // this.dashboard.push(
      //   { cols: 1, rows: 6, y: 0, x: 0 }
      // );
    }
  }

  removeChart(e: any, item: any) {
    this.charts.splice(this.charts.indexOf(item), 1);
  }

  minimizeItem(e: any, item: { rows: number; }) {
    item.rows = 1;
    // this.options.api.optionsChanged(); // MIKEY NOTE: this makes things going in Gridster2!
  }

}
